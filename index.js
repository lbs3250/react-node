const express = require('express');
const path = require('path');
const cors = require("cors");
const app = express();
const multer = require('multer');
const axios = require("axios");
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// Serve the static files from the React app
app.use(express.static(__dirname));
app.use(
	cors({
		origin: ['http://localhost:3000'],
		credentials: true,
	}),
);
var storage = multer.diskStorage({
	destination: function (req, file, cb)
	{
		console.log(req.path)
		cb(null, req.path)
	},
	filename: function (req, file, cb)
	{
		cb(null, file.originalname)

	},
	encoding: "application/x-zip-compressed",


})

var upload = multer({ storage: storage }).single('file')
// An api endpoint that returns a short list of items

app.post('/home/importer/*', (req, res) =>
{
	//	console.log(req);
	//	console.log(res);
	upload(req, res, function (err)
	{
		if (err instanceof multer.MulterError)
		{

			return res.status(500).json(err)
		} else if (err)
		{
			return res.status(500).json(err)
		}
		return res.status(200).send(req.file)
	});
})
// Handles any requests that don't match the ones above



const getSubMatrixList = (mid, token) =>
	axios.post(
		token + '/data-read',
		{
			columns: [
				{
					aid: 33,
					attribute: 'Id',
					unit_id: 0,
				},
				{
					aid: 33,
					attribute: 'Name',
					unit_id: 0,
				},
			],
			row_limit: 0,
			row_start: 0,
			values_limit: 0,
			values_start: 0,

			where: [
				{
					condition: {
						aid: 33,
						attribute: 'Measurement',
						unit_id: 0,
						operator: 'OP_EQ',
						string_array: {
							values: [mid],
						},
					},
				},
			],
		}
		,
		{
			auth: {
				username: 'admin',
				password: 'admin',
			},
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);

const subMatrix = (mid, token) => axios.post(token + '/valuematrix-read',
	{
		"aid": 33,
		"attributes": [
			"name",
			"values",
			"flags"
		],

		"columns": [{
			"name": "*",
			"unit_id": 0
		}],
		"iid": parseInt(mid),
		"values_limit": 0,
		"values_start": 0

	},
	{
		auth: {
			username: 'admin',
			password: 'admin',
		},
		headers: {
			'Content-Type': 'application/json',
		}
	}



);

const getUnitList = (token, username, password) =>
    axios.post(
        token + '/data-read',
        {

            "columns": [
                {
                    "aid": 26,
                    "attribute": "Id",
                    "unit_id": 0
    
                },
                {
                    "aid": 26,
                    "attribute": "Name",
                    "unit_id": 0
    
                }
    
            ],
    
            "row_limit": 0,
            "row_start": 0,
            "values_limit": 0,
            "values_start": 0
    
    
    
    
        }
        ,
        {
            auth: {
                username: username,
                password: password,
            },
            headers: {
                'Content-Type': 'application/json',
            },
        }
    );



function getDataType(data)
{

	const dataType = data.dataType;
	let type = null;
	switch (dataType)
	{
		case 'DT_DOUBLE':
			type = data.doubleArray
			break;
		case 'DT_DATE':
			type = data.stringArray
			break;
		case 'DT_FLOAT':
			type = data.floatArray
			break;
		case 'DT_LONG':
			type = data.longArray
			break;
		case 'DT_LONGLONG':
			type = data.longlongArray
			break;
		case 'DT_STRING':
			type = data.stringArray
			break;
		case 'DT_SHORT':
			type = data.longArray
			break;
		default:
			type = data.unknownArrays
	}

	return type;
}

function IsJsonString(str)
{
	var whatType = JSON.parse(JSON.stringify(str));
	if (typeof whatType == "string")
	{ //유효하지 않은 json
		console.log('this is not json :-( ')
	} else
	{
		console.log('yay this is json!')
	}
}

function isKeyExists(obj, key)
{
	if (obj[key] == undefined)
	{
		return false;
	} else
	{
		return true;
	}
}





app.post("/report/flowsic", (req, res) =>
{

	console.log(req.body.mid);
	console.log(req.body.token);
	//jsonObj = flowsicJson();
	async function flowsicJson()
	{
		let jsonObj = {};




		await getSubMatrixList(req.body.mid, req.body.token).then(async function (r)
		{
			let subMatrixArr = r.data.matrices[0].columns[0].longlongArray.values;
			let subMatrixNmArr = r.data.matrices[0].columns[1].stringArray.values;
			let len = subMatrixArr.length;
			let subIdArr = [];
			let subNameArr = [];

			//console.log(subMatrixArr);
			console.log(subMatrixNmArr);

			for (let i = 0; i < len; ++i)
			{
				if (subMatrixNmArr[i] == "SampleResults" || subMatrixNmArr[i] == "SampleDataConsolidated" || subMatrixNmArr[i] == "PMSampleResults" || subMatrixNmArr[i] == "CycleResults" || subMatrixNmArr[i] == "PhaseResults" || subMatrixNmArr[i] == "CustomFields" || subMatrixNmArr[i] == "TestDetails" || subMatrixNmArr[i] == "Phases")
				{

					subIdArr.push(subMatrixArr[i]);
					subNameArr.push(subMatrixNmArr[i]);
				}





			}
			//console.log('subMatrixIdArr');
			//console.log(subIdArr);
			//console.log(subNameArr);

			let answerArr = [];

			for (let i = 0; i < subIdArr.length; ++i)
			{
				/* if(i == 0 || i == 6)
					continue; */

				await subMatrix(subIdArr[i], req.body.token).then(async function (res2)
				{
					console.log(subNameArr[i] + ' : ' + i);
					//console.log(getDataType(res2.data.matrices[0].columns[0]).values);
					let chNmArr = getDataType(res2.data.matrices[0].columns[0]).values;
					let valueArr = getDataType(res2.data.matrices[0].columns[1]).values;
					if (subNameArr[i] == "TestDetails")
					{
						//console.log(chNmArr);
						//console.log(valueArr);
						//console.log(getDataType(valueArr));

					}




					//console.log(chNmArr.length);
					//jsonObj.subNameArr[i] = {}
					//console.log(chNmArr);
					//console.log(valueArr);
					let chObj = {};
					//console.log(chNmArr.length);


					for (let j = 0; j < chNmArr.length; ++j)
					{
						//console.log(chNmArr[j]);

						//for(let k = 0; k < )
						//chObj.chNmArr[j] 
						let valObj = {};
						if (!isKeyExists(getDataType(valueArr[j]), 'values'))
						{
							continue;
						}

						let valArr = getDataType(valueArr[j]).values;



						for (let k = 0; k < valArr.length; ++k)
						{

							if (subNameArr[i] != "TestDetails")
							{



								if (subNameArr[i] == "CustomFields")
								{

									if (chNmArr[j] == 'Answer')
									{
										//console.log(valArr[k]);

										answerArr.push(valArr[k]);
									}




								}
							}

						}




						for (let k = 0; k < valArr.length; ++k)
						{

							if (subNameArr[i] != "TestDetails")
							{



								if (subNameArr[i] == "CustomFields")
								{

									if (chNmArr[j] == 'CustomFieldName')
									{

										//console.log(valArr[k]);
										chObj[valArr[k]] = answerArr[k];
									}




								} else
								{
									valObj[k + 1] = valArr[k];
								}
							}

						}
						console.log(valObj);

						if (subNameArr[i] != "TestDetails")
						{
							chObj[chNmArr[j]] = valObj;
						} else
						{
							chObj[chNmArr[j]] = valArr[0];
						}



					}

					//console.log(chObj);

					jsonObj[subNameArr[i]] = chObj;
					//console.log(jsonObj);
					//IsJsonString(jsonObj);




				}).catch(e => console.log(e + ' : ' + i));

				//console.log(jsonObj);
			}


		}).catch(e => console.log(e));

		return jsonObj;

	}

	let json = flowsicJson();
	json.then(function (res2)
	{

		//console.log(res2);
		res.json(res2);

	});



	//IsJsonString('11');



})

app.post("/report/continuous", (req, res) => {

    console.log(req.body.mid);
    console.log(req.body.token);
    console.log(req.body.username);
    console.log(req.body.password);
    let unitArr = [];
    
    async function continuousJson() {
        let jsonObj = {};

        await getUnitList(req.body.token, req.body.username, req.body.password).then(async function (r) {
            //console.log(r.data.matrices[0].columns[0].longlongArray.values);
            //console.log(r.data.matrices[0].columns[1].stringArray.values);
            unitArr = r.data.matrices[0].columns[1].stringArray.values;


        }).catch(e => console.log(e));


        await getSubMatrixList(req.body.mid, req.body.token, req.body.username, req.body.password).then(async function (r) {
            
            let subMatrixArr = r.data.matrices[0].columns[0].longlongArray.values;
            let subMatrixNmArr = r.data.matrices[0].columns[1].stringArray.values;
            let len = subMatrixArr.length;
            let subIdArr = [];
            let subNameArr = [];

            
            

            for (let i = 0; i < len; ++i) {
                if (subMatrixNmArr[i] == ("ContinuousResults") || (subMatrixNmArr[i].includes("Continuous202") && !subMatrixNmArr[i].includes("ReadBag") && !subMatrixNmArr[i].includes("REECi") && !subMatrixNmArr[i].includes("AnalyzerCalibration"))) {

                    subIdArr.push(subMatrixArr[i]);
                    subNameArr.push(subMatrixNmArr[i]);
                }





            }
            console.log('subMatrixIdArr');
            //console.log(subIdArr);
            //console.log(subNameArr);

            let crArr = [];
            let cArr = [];
            let vcrUnitObj = {};
            let vcUnitObj = {};

            for (let i = 0; i < subIdArr.length; ++i) {

                await subMatrix(subIdArr[i], req.body.token, req.body.username, req.body.password).then(async function (res2) {
                    console.log(subNameArr[i] + ' : ' + i);
                    //console.log(getDataType(res2.data.matrices[0].columns[1]).values.unitId);
                    let chNmArr = getDataType(res2.data.matrices[0].columns[0]).values;
                    let valueArr = getDataType(res2.data.matrices[0].columns[1]).values;
                    
                    
                    let chObj = {};

                    console.log(chNmArr);
                    //console.log(valueArr);

                    for (let j = 0; j < chNmArr.length; ++j) {

                        let valObj = {};
                        if (!isKeyExists(getDataType(valueArr[j]), 'values')) {
                            continue;
                        }

                        let valArr = getDataType(valueArr[j]).values;
                        //console.log(chNmArr[j] + ' : ' + valueArr[j].unitId);
                        if(i == 0){
                            vcUnitObj[chNmArr[j]] = unitArr[valueArr[j].unitId - 1];

                        }else if(i == 1){
                            vcrUnitObj[chNmArr[j]] = unitArr[valueArr[j].unitId - 1];

                        }

                        for (let k = 0; k < valArr.length; ++k) {
                            
                            if(j == 0){
                                let vObj = {};
                                
                                
                                //console.log(chNmArr[j] + ' : ' + valueArr[j].unitId);
                                
                                vObj[chNmArr[j]] = valArr[k];

                                if(i == 0){
                                    //crArr.push(vUnitObj);
                                    cArr.push(vObj);
                                    
    
                                }else if(i == 1){
                                    
                                    //cArr.push(vUnitObj);
                                    crArr.push(vObj);
    
                                }

                            }else{
                                if(i == 0){
                                    cArr[k][chNmArr[j]] = valArr[k];
    
                                }else if(i ==1){
                                    crArr[k][chNmArr[j]] = valArr[k];
    
                                }

                            }
                            
                            


                        }

                    }

                    




                }).catch(e => console.log(e + ' : ' + i));

                //console.log(jsonObj);

                //jsonObj[subNameArr[i]] = 
            }

            //console.log(crArr);
            //console.log(cArr);

            
            jsonObj['Continuous'] = crArr;
            jsonObj['ContinuousResults'] = cArr;

            //jsonObj['Continuous'].unshift(vcrUnitObj);
            jsonObj['ContinuousResults'].unshift(vcUnitObj);
            

            //jsonObj['ContinuousResults'] = vcrUnitObj;
            //jsonObj['Continuous'] = vcUnitObj;

            //jsonObj['ContinuousResults'].push(crArr);
            //jsonObj['Continuous'].push(cArr);

            //console.log(vcrUnitObj);
            //console.log(vcUnitObj);


        }).catch(e => console.log(e));

        return jsonObj;

    }

    let json = continuousJson();

    json.then(function (res2) {

        //console.log(res2);
        res.json(res2);

    });










    //IsJsonString('11');




})


app.post("/api/common/sendmail", (req, res) => {
	let param = {
		  toEmail: req.body.toEmail
		//, fromEmail: req.body.fromEmail
		, subject: req.body.subject
		, html: req.body.html
		//, text: req.body.text
	};

	var transporter = nodemailer.createTransport({
		service: 'Outlook365'
		, prot: 587
		, host: 'smtp.office365.com'
		//, secure: false
		, auth: {
			user: 'bigdata@tenergy.co.kr'
			, pass: '12!@team'

		}
	});

	// 메일 옵션
	var mailOptions = {
		sender: "BigData <bigdata@tenergy.co.kr>" //보내는 이메일		
		, to: param.toEmail // 수신할 이메일
		, subject: param.subject // 메일 제목
		, html: param.html // 메일 내용(html)
		//,text: param.text // 메일 내용(텍스트)
	};

	// 메일 발송    
	transporter.sendMail(mailOptions, function (error, info) {
		if (error) {
			//console.log(error);
			res.json({ status: "error", code: error.responseCode });
		} else {
			//console.log(info);
			res.json({ status: "success", code: "succ" });
		}
	});
})

app.get("/api/approve/project" , (req, res) => {
    //to-do
	//0. request 파라미터 값 유효성 확인
    //1. tokken, coni 값 취득을 위해 임의 로그인 프로세스(admin, admin) 삽입
    //2. 해당 project 가 이미 승인 되었는지 확인
    //3. 요청 데이터가 없거나 이미 승인되었다면 알림 return
    //4. 승인 되지 않았다면 데이터 업데이트 프로세스 진행
	//5. 승인 완료 시 시험담당자에게 메일 발송
    //6. 승인 완료 알림 return

	if(!req.query.id || req.query.id == ""){
		let warnningMessage = '<p>프로젝트 승인 호출 시 오류가 발생하였습니다.</p>';
		warnningMessage += '<p>사이트 담당자에게 확인 요청하십시오.</p>';

		res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
		res.end(warnningMessage);
		return;
	}

	const args = { url:"", projectId: "", conI: "", userName: "", password: ""};
	args.url = "http://192.168.22.167:3333";
	args.projectId = req.query.id;
	args.userName = "admin";
	args.password = "admin";

	let returnMessage = ""; //프로세스 수행 결과에 따라 유저에게 리턴할 메세지 정의
	let _isApproved = false; //승인 여부 : true-시험담당자에게 메일 발송

	Main(); //메인함수 실행

	async function Main() {
		let getProjectStatusResult = ""; //프로젝트 정보

		try {
			const localLoginResult = await LocalLogin(args);
			args.conI = localLoginResult.headers.location.split('/')[4];
			
			getProjectStatusResult = await GetProejctStatus(args);
			const projectData = getProjectStatusResult.data.matrices[0].columns[0].stringArray.values;//DATA-READ 결과 PROJECT.USEYN
			if (projectData) {
				let useYn = projectData[0]; //key 값으로 조회해서 무조건 1개
				if (useYn == "Y") {					
					//이미 승인됨
					returnMessage = "요청하신 프로젝트는 이미 승인 되었습니다.";
				} else {
					//승인 대기 : 승인 프로세스 진행
					const updateProjectResult = await UpdateProject(args);

					if(updateProjectResult){
						//승인 완료
						returnMessage = "신규 프로젝트 생성 승인 되었습니다.";
						_isApproved = true;
					}else{
						//승인 실패
						returnMessage = "신규 프로젝트 생성 승인 중 오류가 발생하였습니다.";
					}
				}
			} else {
				//프로젝트 데이터 없음
				returnMessage = "요청하신 프로젝트 데이터가 존재하지 않습니다.";
			}
		} catch (error) {
			console.log("Error : " + error);
			returnMessage = "알수 없는 오류가 발생하였습니다. Error : " + error;
		}

		let responseMessage = '<p>' + returnMessage + '</p>';		
		//responseMessage += '<p><u><strong><a href="http://localhost:8888">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>'; //디버깅
		responseMessage += '<p><u><strong><a href="http://tminemdm.co.kr/">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>';

		if(_isApproved){
			//시험담당자에게 메일발송
			try {
				const projectName = getProjectStatusResult.data.matrices[0].columns[1].stringArray.values[0];//project.name
				let testerList = getProjectStatusResult.data.matrices[0].columns[2].stringArray.values[0].split('_');//project.testowner
				let tempTesterList = testerList.shift(); //배열 첫번째 값 버림(공백)

				if (testerList.length > 0) {
					const getUserDataResult = await GetUserData(args, testerList); //user.email
					const testerEmailList = getUserDataResult.data.matrices[0].columns[0].stringArray.values;

					let receiveAddress = "";

					for (var i = 0; i < testerEmailList.length; i++) {
						if (testerEmailList[i] != "") {
							receiveAddress += testerEmailList[i];
							receiveAddress += ";";
						}
					}

					if (receiveAddress != "") {
						//받는 사람이 있을 경우
						let mailMessage = '<p>시험 담당자님, 신규 프로젝트(' + projectName + ')가 사용 승인 되었습니다.</p>';
						//responseMessage += '<p><u><strong><a href="http://localhost:8888">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>'; //디버깅
						mailMessage += '<p><u><strong><a href="http://tminemdm.co.kr/">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>';

						const mailArgs = { toEmail: receiveAddress, subject: "[TMine]신규 프로젝트 생성 승인 완료 알림", html: mailMessage };
						const sendMailResult = await SendMail(mailArgs);
						console.log(sendMailResult);
					}
				}
			} catch (error) {
				console.log("Error mail send : " + error); //ignore
			}
		}

		if (args.conI != "") {
			//모든 작업 끝내고 로그아웃 수행
			try {
				const localLogoutResult = await LocalLogout(args);
			} catch (error) {
				console.log("Error logout : " + error); //ignore
			}
		}

		res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
		res.end(responseMessage);
	}
})

app.get("/api/approve/test" , (req, res) => {
    //to-do
	//0. request 파라미터 값 유효성 확인
    //1. tokken, coni 값 취득을 위해 임의 로그인 프로세스(admin, admin) 삽입
    //2. 해당 test 가 이미 삭제 되었는지 확인
    //3. 요청 데이터가 없거나 이미 삭제되었다면 알림 return
    //4. 삭제 되지 않았다면 데이터 업데이트 프로세스 진행
	//5. 삭제 완료 시 삭제 요청자에게 메일 발송
    //6. 승인 완료 알림 return

	if(!req.query.id || req.query.id == ""){
		let warnningMessage = '<p>테스트 데이터 삭제 승인 호출 시 오류가 발생하였습니다.</p>';
		warnningMessage += '<p>사이트 담당자에게 확인 요청하십시오.</p>';

		res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
		res.end(warnningMessage);
		return;
	}

	const args = { url:"", testId: "", conI: "", userName: "", password: ""};
	args.url = "http://192.168.22.167:3333";
	args.testId = req.query.id;
	args.userName = "admin";
	args.password = "admin";

	let returnMessage = ""; //프로세스 수행 결과에 따라 유저에게 리턴할 메세지 정의
	let _isApproved = false; //승인 여부 : true-삭제 요청자에게 메일 발송

	Main(); //메인함수 실행

	async function Main() {
		let getTestStatusResult = ""; //테스트 정보

		try {
			const localLoginResult = await LocalLogin(args);
			args.conI = localLoginResult.headers.location.split('/')[4];
			
			getTestStatusResult = await GetTestStatus(args);
			const testData = getTestStatusResult.data.matrices[0].columns[0].stringArray.values;//DATA-READ 결과 TEST.USEYN
			if (testData) {
				let useYn = testData[0]; //key 값으로 조회해서 무조건 1개
				if (useYn == "N") {					
					//이미 삭제됨
					returnMessage = "요청하신 테스트 데이터는 이미 삭제 되었습니다.";
				} else {
					//삭제 프로세스 진행
					const updateTestResult = await UpdateTest(args);

					if(updateTestResult){
						//삭제 완료
						returnMessage = "테스트 데이터가 삭제 되었습니다.";
						_isApproved = true;
					}else{
						//삭제 실패
						returnMessage = "테스트 데이터 삭제 중 오류가 발생하였습니다.";
					}
				}
			} else {
				//테스트 데이터 없음
				returnMessage = "요청하신 테스트 데이터가 존재하지 않습니다.";
			}
		} catch (error) {
			console.log("Error : " + error);
			returnMessage = "알수 없는 오류가 발생하였습니다. Error : " + error;
		}

		let responseMessage = '<p>' + returnMessage + '</p>';		
		//responseMessage += '<p><u><strong><a href="http://localhost:8888">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>'; //디버깅
		responseMessage += '<p><u><strong><a href="http://tminemdm.co.kr/">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>';

		if(_isApproved){
			//삭제 요청자 메일발송
			try {
				const testName = getTestStatusResult.data.matrices[0].columns[1].stringArray.values[0];//test.name				

				if(req.query.user || req.query.user != ""){
					const getUserDataResult = await GetUserData(args, [req.query.user]); //user.email
					const userEmailList = getUserDataResult.data.matrices[0].columns[0].stringArray.values;

					let receiveAddress = "";

					for (var i = 0; i < userEmailList.length; i++) {
						if (userEmailList[i] != "") {
							receiveAddress += userEmailList[i];
							receiveAddress += ";";
						}
					}

					if (receiveAddress != "") {
						//받는 사람이 있을 경우
						let mailMessage = '<p>테스트 데이터(' + testName + ') 삭제 요청 건이 승인 되었습니다.</p>';
						mailMessage += '<p><u><strong><a href="http://tminemdm.co.kr/">TMine</a></strong></u> 으로 이동하여 확인하십시오.</p>';

						const mailArgs = { toEmail: receiveAddress, subject: "[TMine]테스트 데이터 삭제 승인 완료 알림", html: mailMessage };
						const sendMailResult = await SendMail(mailArgs);
						console.log(sendMailResult);
					}
				}				
			} catch (error) {
				console.log("Error mail send : " + error); //ignore
			}
		}

		if (args.conI != "") {
			//모든 작업 끝내고 로그아웃 수행
			try {
				const localLogoutResult = await LocalLogout(args);
			} catch (error) {
				console.log("Error logout : " + error); //ignore
			}
		}

		res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
		res.end(responseMessage);
	}
})

const LocalLogin = (param) =>
	axios.post(
		param.url + '/ods/',
		{},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);

const LocalLogout = (param) =>
	axios.delete(
		param.url + '/ods/' + param.conI,
		{
			auth: {
				username: param.userName,
				password: param.password,
			},

			headers: {
				'Content-Type': 'application/json'
			},
		}
	);

const GetProejctStatus = (param) =>
	axios.post(
		param.url + '/ods/' + param.conI + "/data-read",
		{
			columns: [
				{
					aid: 10,
					attribute: 'useYn',
					unit_id: 0,
				},
				{
					aid: 10,
					attribute: 'Name',
					unit_id: 0,
				},
				{
					aid: 10,
					attribute: 'TestOwner',
					unit_id: 0,
				}
			],
			where: [
				{
					condition: {
						aid: 10,
						attribute: 'Id',
						unit_id: 0,
						operator: 'OP_EQ',
						string_array: {
							values: [param.projectId],
						},
					},
				},
			],
		},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			}
		}
	);

const UpdateProject = (param) =>
	axios.post(
		param.url + '/ods/' + param.conI + "/data-update",
		{
			"matrices": [
				{
					"name": "Project",
					"baseName": "AoProject",
					"aid": 10,
					"columns": [
						{
							"name": "Id",
							"baseName": "id",
							"dataType": "DT_LONGLONG",
							"longlongArray": {
								"values": [Number(param.projectId)],
							},
						},
						{
							"name": "useYn",
							"dataType": "DT_STRING",
							"stringArray": {
								"values": ["Y"]
							}
						},
					]
				}
			]
		},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);

const GetTestStatus = (param) =>
	axios.post(
		param.url + '/ods/' + param.conI + "/data-read",
		{
			columns: [
				{
					aid: 13,
					attribute: 'useYn',
					unit_id: 0,
				},
				{
					aid: 13,
					attribute: 'Name',
					unit_id: 0,
				}
			],
			where: [
				{
					condition: {
						aid: 13,
						attribute: 'Id',
						unit_id: 0,
						operator: 'OP_EQ',
						string_array: {
							values: [param.testId],
						},
					},
				},
			],
		},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			}
		}
	);

const UpdateTest = (param) =>
	axios.post(
		param.url + '/ods/' + param.conI + "/data-update",
		{
			"matrices": [
				{
					"name": "Test",
					"baseName": "AoSubTest",
					"aid": 13,
					"columns": [
						{
							"name": "Id",
							"baseName": "id",
							"dataType": "DT_LONGLONG",
							"longlongArray": {
								"values": [Number(param.testId)],
							},
						},
						{
							"name": "useYn",
							"dataType": "DT_STRING",
							"stringArray": {
								"values": ["N"]
							}
						},
					]
				}
			]
		},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);

const SendMail = async (param) =>
	axios.post('http://localhost:3030/api/common/sendmail',
		{
			//"sender": param.sender,
			"toEmail": param.toEmail,
			"subject": param.subject,
			"html": param.html
		},
		{
			headers: {
				"Access-Control-Allow-Headers": "Content-Type",
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
				'Content-Type': 'application/json'
			}
		}
	);

const GetUserData = (param, whereValues) =>
	axios.post(
		param.url + '/ods/' + param.conI + "/data-read",
		{
			columns: [
				{
					aid: 27,
					attribute: 'Email',
					unit_id: 0,
				}
			],
			where: [
				{
					condition: {
						aid: 27,
						attribute: 'Id',
						unit_id: 0,
						operator: 'OP_INSET',
						string_array: {
							values: whereValues
						},
					},
				},
			],
		},
		{
			auth: {
				username: param.userName,
				password: param.password,
			},
			headers: {
				'Content-Type': 'application/json',
			}
		}
	);

const port = process.env.PORT || 3030;
app.listen(port);

console.log('App is listening on port ' + port);
